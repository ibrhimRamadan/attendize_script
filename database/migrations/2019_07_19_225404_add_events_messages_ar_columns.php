<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventsMessagesArColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('pre_order_display_message_ar', 20)->after('pre_order_display_message')->nullable();
            $table->string('post_order_display_message_ar', 20)->after('post_order_display_message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('pre_order_display_message_ar');
            $table->dropColumn('post_order_display_message_ar');
        });
    }
}
