@php
    $lng = isset($_GET['lng']) && $_GET['lng'] == 'AR' ? 'ar' : app()->getLocale();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"
@endphp



@if(!$event->is_live)
<section id="goLiveBar">
    <div class="container">
        @if(!$event->is_live)

        {{ @trans("ManageEvent.event_not_live",[],$lng) }}
        <a href="{{ route('MakeEventLive' , ['event_id' => $event->id]) }}"
           style="background-color: green; border-color: green;"
        class="btn btn-success btn-xs">{{ @trans("ManageEvent.publish_it",[],$lng) }}</a>
        @endif
    </div>
</section>
@endif

<section id="organiserHead" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div onclick="window.location='{{$event->event_url}}#organiser'" class="event_organizer">
                    @if($lng == 'ar')
                    <b>{{$event->organiser->name_ar}}</b> @lang("Public_ViewEvent.presents",[],$lng)
                    @else
                    <b>{{$event->organiser->name}}</b> @lang("Public_ViewEvent.presents",[],$lng)
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<section id="intro" class="container">
    <div class="row">

      <div>


          @if($_GET && !array_key_exists('lng',$_GET))
              <a class="btn" href="{{$actual_link}}&lng=EN"> EN </a> | <a class="btn" href="{{$actual_link}}&lng=AR"> AR </a>
          @elseif($_GET && array_key_exists('lng',$_GET))
              <a class="btn" href="{{ explode('lng=',$actual_link)[0] }}lng=EN"> EN </a> | <a class="btn" href="{{ explode('lng=',$actual_link)[0] }}lng=AR"> AR </a>
          @else
              <a class="btn" href="?lng=EN"> EN </a> | <a class="btn" href="?lng=AR"> AR </a>
          @endif

      </div>

        <div class="col-md-12">

            @if(isset($_GET['lng']) && $_GET['lng'] =='AR')
            <h1 property="name">{{$event->title_ar}}</h1>
            @else
            <h1 property="name">{{$event->title}}</h1>
            @endif

            <div class="event_venue">
                <span property="startDate" content="{{ $event->start_date->toIso8601String() }}">
                    {{ $event->startDateFormatted() }}
                </span>
                -
                <span property="endDate" content="{{ $event->end_date->toIso8601String() }}">
                     @if($event->start_date->diffInDays($event->end_date) == 0)
                        {{ $event->end_date->format('H:i') }}
                     @else
                        {{ $event->endDateFormatted() }}
                     @endif
                </span>
                @lang("Public_ViewEvent.at")
                <span property="location" typeof="Place">
                    <b property="name">{{$event->venue_name}}</b>
                    <meta property="address" content="{{ urldecode($event->venue_name) }}">
                </span>
            </div>

            <div class="event_buttons">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <a class="btn btn-event-link btn-lg" href="{{{$event->event_url}}}#tickets">@lang("Public_ViewEvent.TICKETS",[],$lng)</a>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <a class="btn btn-event-link btn-lg" href="{{{$event->event_url}}}#details">@lang("Public_ViewEvent.DETAILS",[],$lng)</a>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <a class="btn btn-event-link btn-lg" href="{{{$event->event_url}}}#location">@lang("Public_ViewEvent.LOCATION",[],$lng)</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
