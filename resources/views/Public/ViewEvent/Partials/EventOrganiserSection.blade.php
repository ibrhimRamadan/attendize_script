@php
$org_name = $lng == 'ar' ? $event->organiser->name_ar : $event->organiser->name;
@endphp
<section id="organiser" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="event_organiser_details" property="organizer" typeof="Organization">
                <div class="logo">
                    <img alt="{{$event->organiser->name}}" src="{{asset($event->organiser->full_logo_path)}}" property="logo">
                </div>
                    @if($event->organiser->enable_organiser_page)
                    <a href="{{route('showOrganiserHome', [$event->organiser->id, Str::slug($event->organiser->name)])}}" title="Organiser Page">
                        {{$org_name}}
                    </a>
                    @else
                        {{$org_name}}
                    @endif
                </h3>

                @if($lng == 'ar')
                <p property="description">
                    {!! nl2br($event->organiser->about_ar)!!}
                </p>
                @else
                <p property="description">
                    {!! nl2br($event->organiser->about)!!}
                </p>
                @endif
                <p>
                    @if($event->organiser->facebook)
                        <a property="sameAs" href="https://fb.com/{{$event->organiser->facebook}}" class="btn btn-facebook">
                            <i class="ico-facebook"></i>&nbsp; @lang("Public_ViewEvent.Facebook")
                        </a>
                    @endif
                        @if($event->organiser->twitter)
                            <a property="sameAs" href="https://twitter.com/{{$event->organiser->twitter}}" class="btn btn-twitter">
                                <i class="ico-twitter"></i>&nbsp; @lang("Public_ViewEvent.Twitter")
                            </a>
                        @endif
                    <button onclick="$(function(){ $('.contact_form').slideToggle(); });" type="button" class="btn btn-primary">
                        <i class="ico-envelop"></i>&nbsp; @lang("Public_ViewEvent.Contact",[],$lng)
                    </button>
                </p>
                <div class="contact_form well well-sm">
                    {!! Form::open(array('url' => route('postContactOrganiser', array('event_id' => $event->id)), 'class' => 'reset ajax')) !!}
                    @if($lng=='ar')
                    <h3>@lang("Public_ViewEvent.Contact",[],$lng) <i>{{$event->organiser->name_ar}}</i></h3>
                    @else
                    <h3>@lang("Public_ViewEvent.Contact",[],$lng) <i>{{$event->organiser->name}}</i></h3>
                    @endif
                    <div class="form-group">
                        {!! Form::label(trans("Public_ViewEvent.your_name",[],$lng)) !!}
                        {!! Form::text('name', null,
                            array('required',
                                  'class'=>'form-control',
                                  'placeholder'=>trans("Public_ViewEvent.your_name",[],$lng))) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label(trans("Public_ViewEvent.your_email_address",[],$lng)) !!}
                        {!! Form::text('email', null,
                            array('required',
                                  'class'=>'form-control',
                                  'placeholder'=>trans("Public_ViewEvent.your_email_address",[],$lng))) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label(trans("Public_ViewEvent.your_message",[],$lng)) !!}
                        {!! Form::textarea('message', null,
                            array('required',
                                  'class'=>'form-control',
                                  'placeholder'=>trans("Public_ViewEvent.your_message",[],$lng))) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit(trans("Public_ViewEvent.send_message_submit",[],$lng),
                          array('class'=>'btn btn-primary')) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>

